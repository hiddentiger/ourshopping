import ReactOnRails from 'react-on-rails';

import OurShoppingApp from '../bundles/OurShopping/startup/OurShoppingApp';

// This is how react_on_rails can see the OurShoppingApp in the browser.
ReactOnRails.register({
  OurShoppingApp,
});
