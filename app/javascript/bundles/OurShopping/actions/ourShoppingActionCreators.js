/* eslint-disable import/prefer-default-export */
import requestsManager from '../../../libs/requestManager';

import { OUR_SHOPPING_NAME_UPDATE } from '../constants/ourShoppingConstants';

export const updateName = (text) => ({
  type: OUR_SHOPPING_NAME_UPDATE,
  text,
});


export function submitComment(list) {
  return (dispatch) => {
    dispatch(setIsSaving());
    return (
      requestsManager
        .submitEntity({ list })
        .then(res => dispatch(submitCommentSuccess(res.data)))
        .catch(error => dispatch(submitCommentFailure(error)))
    );
  };
}
