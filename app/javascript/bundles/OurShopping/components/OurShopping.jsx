import PropTypes from 'prop-types';
import React from 'react';
import request from 'axios';
import Immutable from 'immutable';
import _ from 'lodash';

export default class OurShopping extends React.Component {
  static propTypes = {
    name: PropTypes.string.isRequired, // this is passed from the Rails view
  };

  /**
   * @param props - Comes from your rails view.
   */
  constructor(props) {
    super(props);

    // How to set initial state in ES6 class syntax
    // https://facebook.github.io/react/docs/reusable-components.html#es6-classes
    // this.state = { name: this.props.name };
    this.state = {
      $$lists: Immutable.fromJS([]),
      isSaving: false,
      fetchCommentsError: null,
      submitCommentError: null,
      name: this.props.name
    };

    _.bindAll(this, 'fetchLists', 'handleListSubmit');
  }

  componentDidMount() {
    this.fetchLists();
  }

  handleListSubmit(list) {
    this.setState({ isSaving: true });

    const requestConfig = {
      responseType: 'json',
      headers: ReactOnRails.authenticityHeaders(),
    };

    return (
      request
        .post('lists.json', { list }, requestConfig)
        .then(() => {
          const { $$lists } = this.state;
          const $$list = Immutable.fromJS(list);

          this.setState({
            $$lists: $$lists.unshift($$list),
            submitCommentError: null,
            isSaving: false,
          });
        })
        .catch(error => {
          this.setState({
            submitCommentError: error,
            isSaving: false,
          });
        })
    );
  }

  fetchLists() {
    return (
      request
        .get('lists.json', { responseType: 'json' })
        .then(res => this.setState({ $$lists: Immutable.fromJS(res.data.lists) }))
        .catch(error => this.setState({ fetchListsError: error }))
    );
  }

  updateName = (name) => {
    this.setState({ name });
  };

  sendName = (name) => {
    this.handleListSubmit({name: this.state.name})
      // .submitComment(this.state.list)
      .then(this.resetAndFocus);
  };

  render() {
    return (
      <div>
        <h3>
          Hello, {this.state.name}!
        </h3>
        <hr />
        <form >
          <label htmlFor="name">
            Name of your list:
          </label>
          <input
            id="name"
            type="text"
            value={this.state.name}
            onChange={(e) => this.updateName(e.target.value)}
          />
          <button type="button" onClick={(e) => this.sendName(this.state.name)} >Create new list</button>
        </form>
        <h3>Lists</h3>
        { this.state.$$lists.map(($$list, index) =>
          <p key={index}>name: {$$list.get('name')}</p>
        )}
      </div>
    );
  }
}
