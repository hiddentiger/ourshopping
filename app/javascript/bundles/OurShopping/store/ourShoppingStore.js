import { createStore } from 'redux';
import ourShoppingReducer from '../reducers/ourShoppingReducer';

const configureStore = (railsProps) => (
  createStore(ourShoppingReducer, railsProps)
);

export default configureStore;
