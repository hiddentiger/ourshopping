# frozen_string_literal: true

class OurShoppingController < ApplicationController
  layout "our_shopping"
  before_action :authenticate_user

  def index
    @hello_world_props = { name: "Stranger 2" }
  end
end
