class UserRegistrationController < ApplicationController

  skip_before_action :verify_authenticity_token
  
  def register
   #params.require(:user).permit(:firstname, :email, :password, :password_confirmation)
          # params[:user]a
    user = User.create(user_params)
    # TODO: return JSON
    if user.save
      render json: {status: :ok}
    else
      render json: {status: :error}
    end
  end

   def user_params
      params.require(:user).permit(:first_name, :email, :password, :password_confirmation)
    end

end
