Rails.application.routes.draw do
  resources :items
  post 'user_token' => 'user_token#create'
  post 'register' => 'user_registration#register'
  root 'our_shopping#index'
  get 'hello_world', to: 'hello_world#index'
  get 'our_shopping', to: 'our_shopping#index'
  resources :lists
  # resources :users
  # mount Knock::Engine => "/sessions"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
