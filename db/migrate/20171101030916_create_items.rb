class CreateItems < ActiveRecord::Migration[5.1]
  def change
    create_table :items do |t|
      t.references :lists, foreign_key: true
      t.string :name
      t.integer :quantity
      t.bool :done

      t.timestamps
    end
  end
end
